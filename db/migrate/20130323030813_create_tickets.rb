class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.date :cdate
      t.string :title
      t.string :desc
      t.integer :person_id

      t.timestamps
    end
  end
end
