class Person < ActiveRecord::Base
  attr_accessible :fname, :lname, :phone
  has_many :tickets

  def name
    fname + " " + lname
  end
end
