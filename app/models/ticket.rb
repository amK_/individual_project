class Ticket < ActiveRecord::Base
  attr_accessible :cdate, :desc, :person_id, :title
  belongs_to :person
  #acts_as_solr

  def self.searchTickets(query)
    if (query)
      find(:all, :conditions => ['title LIKE ?', "%#{query}%"])
    else
      find(:all)
    end
  end
end
